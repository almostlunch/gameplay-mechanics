﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnimationManager : MonoBehaviour
{
    private GameObject rifle;
    private GameObject shotgun;
    private GameObject pistol;
    
    //Rifle animations
    public AnimationClip riflePutAway;
    public AnimationClip rifleGetOut;

    //Shotgun animations
    public AnimationClip shotgunPutAway;
    public AnimationClip shotgunGetOut;

    //Pistol animations
    public AnimationClip pistolPutAway;
    public AnimationClip pistolGetOut;

    void Start()
    {
        rifle = gameObject.GetComponent<GunGO>().GetRifle();
        shotgun = gameObject.GetComponent<GunGO>().GetShotgun();
        pistol = gameObject.GetComponent<GunGO>().GetPistol();

        rifle.GetComponent<Animator>().Play("Rifle_GetOut");
    }

    public IEnumerator ChangeToRifle(int activeWeapon)
    {
        if (activeWeapon == 2)
        {
            shotgun.GetComponent<Animator>().Play("Shotgun_PutAway");
            yield return new WaitForSeconds(shotgunPutAway.length);
        }
        else if (activeWeapon == 3)
        {
            pistol.GetComponent<Animator>().Play("Pistol_PutAway");
            yield return new WaitForSeconds(pistolPutAway.length);
        }

        gameObject.GetComponent<InputManager>().ChangeToRifle();

        rifle.GetComponent<Animator>().Play("Rifle_GetOut");
        yield return new WaitForSeconds(rifleGetOut.length);
        gameObject.GetComponent<GunManager>().SetCanChange(true);
    }

    public IEnumerator ChangeToShotgun(int activeWeapon)
    {
        if (activeWeapon == 1)
        {
            rifle.GetComponent<Animator>().Play("Rifle_PutAway");
            yield return new WaitForSeconds(riflePutAway.length);
        }
        else if (activeWeapon == 3)
        {
            pistol.GetComponent<Animator>().Play("Pistol_PutAway");
            yield return new WaitForSeconds(pistolPutAway.length);
        }

        gameObject.GetComponent<InputManager>().ChangeToShotgun();

        shotgun.GetComponent<Animator>().Play("Shotgun_GetOut");
        yield return new WaitForSeconds(shotgunGetOut.length);
        gameObject.GetComponent<GunManager>().SetCanChange(true);
    }

    public IEnumerator ChangeToPistol(int activeWeapon)
    {
        if (activeWeapon == 1)
        {
            rifle.GetComponent<Animator>().Play("Rifle_PutAway");
            yield return new WaitForSeconds(riflePutAway.length);
        }
        else if (activeWeapon == 2)
        {
            shotgun.GetComponent<Animator>().Play("Shotgun_PutAway");
            yield return new WaitForSeconds(shotgunPutAway.length);
        }

        gameObject.GetComponent<InputManager>().ChangeToPistol();

        pistol.GetComponent<Animator>().Play("Pistol_GetOut");
        yield return new WaitForSeconds(pistolGetOut.length);
        gameObject.GetComponent<GunManager>().SetCanChange(true);
    }

    public void StartRifleAnim()
    {
        if (rifle != null)
        {
            rifle.GetComponent<Animator>().Play("Rifle_GetOut");
        }
    }
}