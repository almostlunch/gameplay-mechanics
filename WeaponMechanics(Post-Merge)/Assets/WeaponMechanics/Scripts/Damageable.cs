﻿using UnityEngine;
using System.Collections;

public class Damageable : MonoBehaviour
{
    protected GameObject scriptManager;

    protected int health;
    protected int bleedStacks = 0;
    protected float bleedStackDur = 4.0f;
    protected float bleedTick = 0.5f;

    protected int fireStacks = 0;
    protected int fireStacksMax = 10;
    protected float fireTick = 0.5f;

    void Start()
    {
        health = 20;
        scriptManager = GameObject.FindGameObjectWithTag("ScriptManager");
    }

    void Update()
    {
        if (bleedStacks > 0)
        {
            bleedStackDur -= Time.deltaTime;
            bleedTick -= Time.deltaTime;
        }

        if (bleedStackDur < 0.0f)
        {
            bleedStacks--;
            bleedStackDur = 4.0f;
        }

        if (bleedTick < 0.0f)
        {
            health -= 5;
            bleedTick = 0.5f;
        }

        //Fire
        if (fireStacks > 0)
            bleedTick -= Time.deltaTime;

        if (bleedTick < 0.0f)
        {
            health -= fireStacks;
            fireTick = 0.5f;
        }

        if (fireStacks > fireStacksMax)
            fireStacks = fireStacksMax;
    }

    public void TakeDamage(int dmg)
    {
        health -= dmg;

        if (health <= 0)
        {
            int activeWeapon = scriptManager.GetComponent<GunManager>().GetActiveWeapon();

            if (activeWeapon == 1)
                scriptManager.GetComponent<GunGO>().GetRifle().GetComponent<Gun>().GainExp(1000);
            else if (activeWeapon == 2)
                scriptManager.GetComponent<GunGO>().GetShotgun().GetComponent<Gun>().GainExp(1000);
            else if (activeWeapon == 3)
                scriptManager.GetComponent<GunGO>().GetPistol().GetComponent<Gun>().GainExp(1000);

            Destroy(gameObject);
        }
    }

    public void AddBleedStack(int no)
    {
        bleedStacks += no;
    }

    public void Cripple()
    {
        //DO CRIPPLE STUFF HERE
    }

    public void AddFireStack(int no)
    {
        fireStacks += no;
    }

    public void Kill()
    {
        Destroy(gameObject);
    }
}