﻿using UnityEngine;
using System.Collections;

public class DestroyAtTime : MonoBehaviour
{
    public float timer;

    void Start()
    {
        Destroy(gameObject, timer);
    }
}