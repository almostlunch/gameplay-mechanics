﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour
{
    GameObject m_character;
    public GameObject m_camera;
    private GameObject rifle;
    private GameObject shotgun;
    private GameObject pistol;

    private bool jetPackDelay = false;

    void Start()
    {
        rifle = gameObject.GetComponent<GunGO>().GetRifle();
        shotgun = gameObject.GetComponent<GunGO>().GetShotgun();
        pistol = gameObject.GetComponent<GunGO>().GetPistol();

        m_character = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        if (!gameObject.GetComponent<MenuManager>().GetIsMenuOpen() && !gameObject.GetComponent<MenuManager>().GetIsExitOpen())
        {
            Move();
            Sprint();
            Jump();
            JetPack();
            ShootTest();
            ChangeWeapon();
            ThrowGrenade();
        }
        MenuScreen();
    }

    void Move()
    {
        if (Input.GetKey(KeyCode.W))
        {
            m_character.GetComponent<PlayerController>().Move("Forward");
        }
        if (Input.GetKey(KeyCode.A))
        {
            m_character.GetComponent<PlayerController>().Move("Left");
        }
        if (Input.GetKey(KeyCode.S))
        {
            m_character.GetComponent<PlayerController>().Move("Back");
        }
        if (Input.GetKey(KeyCode.D))
        {
            m_character.GetComponent<PlayerController>().Move("Right");
        }
    }

    void Sprint()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            m_character.GetComponent<PlayerController>().Sprint(true);
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            m_character.GetComponent<PlayerController>().Sprint(false);
        }
    }

    void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (m_character.GetComponent<PlayerController>().GetIsGrounded() == true)
            {
                m_character.GetComponent<PlayerController>().Jump();
                StartCoroutine(JetPackDelay());
            }
        }
    }

    void JetPack()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            if (m_character.GetComponent<PlayerController>().GetIsGrounded() == false)
            {
                if (jetPackDelay == true)
                {
                    if (m_character.GetComponent<JetpackCharge>().GetJetpackCharge() > 0.0f)
                    {
                        m_character.GetComponent<PlayerController>().JetPack();
                        m_character.GetComponent<JetpackCharge>().UseJetpack();
                    }
                }
            }
        }

        if (m_character.GetComponent<PlayerController>().GetIsGrounded() == true)
        {
            if (jetPackDelay)
                jetPackDelay = false;
        }
    }

    void ShootTest()
    {
        if (gameObject.GetComponent<GunManager>().GetActiveWeapon() == 1)
        {
            if (Input.GetMouseButton(0))
            {
                rifle.GetComponent<Rifle>().Shoot();
            }
        }
        else if (gameObject.GetComponent<GunManager>().GetActiveWeapon() == 2)
        {
            if (Input.GetMouseButton(0))
            {
                if (shotgun.GetComponent<Shotgun>().GetIsShotgunAug("Flame"))
                    shotgun.GetComponent<Shotgun>().ShootFlame();
                else if (shotgun.GetComponent<Shotgun>().GetIsShotgunAug("Grenade"))
                    shotgun.GetComponent<Shotgun>().ShootGrenade();
                else
                    shotgun.GetComponent<Shotgun>().Shoot();
            }
        }
        else if (gameObject.GetComponent<GunManager>().GetActiveWeapon() == 3)
        {
            if (Input.GetMouseButton(0))
            {
                pistol.GetComponent<Pistol>().Shoot();
            }
        }
    }

    void ChangeWeapon()
    {
        if (gameObject.GetComponent<GunManager>().GetCanChange())
        {
            if (Input.GetKey(KeyCode.Alpha1))
            {
                if (gameObject.GetComponent<GunManager>().GetActiveWeapon() != 1)
                {
                    StartCoroutine(gameObject.GetComponent<AnimationManager>().ChangeToRifle(gameObject.GetComponent<GunManager>().GetActiveWeapon()));
                }
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                if (gameObject.GetComponent<GunManager>().GetActiveWeapon() != 2)
                {
                    StartCoroutine(gameObject.GetComponent<AnimationManager>().ChangeToShotgun(gameObject.GetComponent<GunManager>().GetActiveWeapon()));
                }
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                if (gameObject.GetComponent<GunManager>().GetActiveWeapon() != 3)
                {
                    StartCoroutine(gameObject.GetComponent<AnimationManager>().ChangeToPistol(gameObject.GetComponent<GunManager>().GetActiveWeapon()));
                }
            }
        }
    }

    void MenuScreen()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (gameObject.GetComponent<MenuManager>().GetIsMenuOpen())
            {
                gameObject.GetComponent<MenuManager>().CloseMenu();
            }
            else
            {
                gameObject.GetComponent<MenuManager>().OpenMenu();
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gameObject.GetComponent<MenuManager>().GetIsExitOpen())
            {
                gameObject.GetComponent<MenuManager>().CloseExit();
            }
            else
            {
                gameObject.GetComponent<MenuManager>().OpenExit();
            }
        }
    }

    public void ChangeToRifle()
    {
        gameObject.GetComponent<GunManager>().ActivateRifle();
    }

    public void ChangeToShotgun()
    {
        gameObject.GetComponent<GunManager>().ActivateShotgun();
    }

    public void ChangeToPistol()
    {
        gameObject.GetComponent<GunManager>().ActivatePistol();
    }

    void ThrowGrenade()
    {
        if (Input.GetKeyUp(KeyCode.G))
        {
            gameObject.GetComponent<GrenadeManager>().ThrowGrenade();
        }
    }

    IEnumerator JetPackDelay()
    {
        yield return new WaitForSeconds(0.4f);
        jetPackDelay = true;
    }
}