﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    Transform m_transform;
    Rigidbody m_rigidbody;
    public Transform m_camera;
    public GameObject theCamera;
    public GameObject scriptManager;

    private const float MAX_SPEED = 18000.0f;
    private const float JUMP_FORCE = 5000.0f;
    private const float JETPACK_SPEED = 5000.0f;
    private const float JETPACK_INIT_SPEED = 200.0f;
    private const float DRAG_FACTOR = 0.93f;

    private bool isGrounded;

    private float speedMultiplier = 1.0f;
    private float speedBuff = 1.0f;
    private float speedBuffTimer = 4.0f;

    private float lookSpeedX = 2.0f;
    private float lookSpeedY = 2.0f;
    private float lookCurrX = 0.0f;
    private float lookCurrY = 0.0f;


    void Start()
    {
        m_transform = transform;
        m_rigidbody = m_transform.gameObject.GetComponent<Rigidbody>();
        //m_camera = GameObject.FindGameObjectWithTag("MainCamera").transform;

        Cursor.lockState = CursorLockMode.Locked;
    }

    void FixedUpdate()
    {
        LookXAxis();
        LookYAxis();
        Drag();

        if (speedBuff != 1.0f)
        {
            if (speedBuffTimer > 0.0f)
            {
                speedBuffTimer -= Time.deltaTime;
            }
            else
            {
                speedBuff = 1.0f;
                speedBuffTimer = 4.0f;
            }
        }
    }

    void OnEnable()
    {
        scriptManager.GetComponent<AnimationManager>().StartRifleAnim();
    }

    public void Move(string direction)
    {
        switch(direction)
        {
            case "Forward":
                m_rigidbody.AddForce(m_transform.forward * MAX_SPEED * speedMultiplier * speedBuff * Time.deltaTime, ForceMode.Force);
                break;

            case "Left":
                m_rigidbody.AddForce(-m_transform.right * MAX_SPEED * speedMultiplier * speedBuff * Time.deltaTime, ForceMode.Force);
                break;

            case "Right":
                m_rigidbody.AddForce(m_transform.right * MAX_SPEED * speedMultiplier * speedBuff * Time.deltaTime, ForceMode.Force);
                break;

            case "Back":
                m_rigidbody.AddForce(-m_transform.forward * MAX_SPEED * speedMultiplier * speedBuff * Time.deltaTime, ForceMode.Force);
                break;

            default:
                Debug.Log("Script 'PlayerController' is calling the default section of switch statement in the Move() function");
                break;


        }
    }

    public void Jump()
    {
        m_rigidbody.AddForce(m_transform.up * Time.deltaTime * JUMP_FORCE, ForceMode.Impulse);
        isGrounded = false;
    }

    public void JetPack()
    {
        m_rigidbody.AddForce(m_transform.up * Time.deltaTime *JETPACK_SPEED, ForceMode.Force);
        m_rigidbody.AddForce(m_transform.up * Time.deltaTime * JETPACK_INIT_SPEED, ForceMode.Impulse);

        //So this may need adjusting depending on how much we allow the jetpack to be used, if left on for over a couple seconds the character will carry on flying upwards for a while even after spacebar has been released
    }

    public void Sprint(bool isSpr)
    {
        if (isSpr)
        {
            speedMultiplier = 1.75f;
        }
        else
        {
            speedMultiplier = 1.0f;
        }
    }

    public void SpeedBuff()
    {
        speedBuff = 1.5f;
    }

    public void LookXAxis()
    {
        lookCurrX += lookSpeedX * Input.GetAxis("Mouse X");

        //lookCurrX -= theCamera.GetComponent<CameraRecoil>().RecoilCalc();

        m_transform.eulerAngles = new Vector3(0.0f, lookCurrX, 0.0f);
    }

    public void LookYAxis()
    {
        lookCurrY -= lookSpeedY * Input.GetAxis("Mouse Y");

        float recoil = theCamera.GetComponent<CameraRecoil>().RecoilCalc();

        lookCurrY = Mathf.Clamp(lookCurrY, -90.0f, 90.0f);

        m_camera.eulerAngles = new Vector3(lookCurrY - recoil, lookCurrX, 0.0f);
        //m_transform.eulerAngles = new Vector3(lookCurrY, lookCurrX, 0.0f);
    }

    public void Drag()
    {
        Vector3 vel;

        vel = m_rigidbody.velocity;

        vel.x *= DRAG_FACTOR;
        vel.z *= DRAG_FACTOR;

        m_rigidbody.velocity = vel;
    }

    public bool GetIsGrounded()
    {
        return isGrounded;
    }

    public void SetIsGrounded(bool grounded)
    {
        this.isGrounded = grounded;
    }
}