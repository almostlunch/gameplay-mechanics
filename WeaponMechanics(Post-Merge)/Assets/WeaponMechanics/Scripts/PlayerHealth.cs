﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour
{
    private int health;

    void Start()
    {
        health = 100;
    }

    public void Heal(int no)
    {
        health += no;

        if (health > 100)
        {
            health = 100;
        }
    }

    public void TakeDamage(int no)
    {
        health -= no;

        if (health < 0)
        {
            //Die
        }
    }
}