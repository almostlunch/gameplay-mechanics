﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public abstract class Gun : MonoBehaviour
{
    public GameObject m_player;
    public GameObject m_camera;
    public GameObject scriptManager;
    public GameObject m_trail;
    public GameObject bulletLeave;
    public GameObject bulletImpact;
    public GameObject muzzleFlash;
    public GameObject parentGun;
    public GameObject parentGunRot;
    public GameObject theCamera;
    public GameObject smoke;
    public GameObject[] movingParts;
    public GameObject grenade;

    public bool canShoot;
    private float shootTimer;

    protected int maxEnergy;
    protected int currEnergy;
    protected int energyUse;
    protected float fireRate;
    protected int damage;
    protected float inaccuracy;
    protected float baseFireRate;
    protected int baseDamage;
    protected float baseInaccuracy;
    protected float inaccuracyLerp;
    protected float inaccuracyMultiplier;
    protected float noOfBulletsPerShot;
    protected float baseNoOfBulletsPerShot;
    protected int multiShot;
    protected int penetrationNo = 1;

    protected float timeGap;

    //Weapon levels
    protected int level;
    protected int exp;
    protected int[] expBracket;
    protected int skillPoints;

    //Weapon skills
    protected Pair<string, int>[] skillTree;

    //Confusing math stuff for bullet trail angles
    protected Vector3 cameraRightAngle;
    protected float offsetTrailAngle;

    //BAD CODE - Need to know shotgun augment, must be changed!
    protected bool isRifleLMG = false;
    protected bool isRifleDMR = false;

    protected bool isShotgunFlame = false;
    protected bool isShotgunGrenade = false;

    public void Start()
    {
        //m_player = GameObject.FindGameObjectWithTag("Player");
        //m_camera = GameObject.FindGameObjectWithTag("MainCamera");
        shootTimer = 0.0f;

        multiShot = 0;

        timeGap = Time.deltaTime;

        level = 30;
        exp = 0;
        skillPoints = 30;
        expBracket = new int[31];
        PopulateExpBracket();
    }

    public void Update()
    {
        if (!canShoot)
        {
            shootTimer += Time.deltaTime;

            if (shootTimer >= fireRate)
            {
                canShoot = true;
                shootTimer = 0.0f;
            }
        }

        //Inaccuracy extra stuff
        if (scriptManager.GetComponent<GunManager>().GetActiveWeapon() != 2)
        {
            inaccuracyLerp -= timeGap;

            if (inaccuracyLerp < 0.0f)
            {
                inaccuracyLerp = 0.0f;
            }
        }

        //Crosshair manipulation
        Crosshair.SetLerpNo(inaccuracyLerp);
        Crosshair.SetMultiplier(inaccuracyMultiplier);
    }

    public void Shoot()
    {
        if (canShoot)
        {
            if (currEnergy > 0)
            {
                //currEnergy -= energyUse;
                AddEnergy(-energyUse);

                multiShot = 0;

                //Deals with procs for buffs
                foreach (string s in ProcBuff())
                {
                    if (s == "NotUseENergy")
                    {
                        AddEnergy(energyUse);
                    }
                    else if (s == "Lifesteal")
                    {
                        m_player.GetComponent<PlayerHealth>().Heal(5);
                    }
                    else if (s == "MoveSpeed")
                    {
                        m_player.GetComponent<PlayerController>().SpeedBuff();
                    }
                    else if (s == "MultiShot")
                    {
                        multiShot++;
                    }
                }

                for (int i = 0; i < noOfBulletsPerShot + multiShot; i++)
                {
                    Vector3 dir = m_camera.transform.rotation * Vector3.forward;
                    Vector3 inaccuracyVector = new Vector3(
                        Random.Range(-inaccuracy * inaccuracyLerp, inaccuracy * inaccuracyLerp), 
                        Random.Range(-inaccuracy * inaccuracyLerp, inaccuracy * inaccuracyLerp), 
                        Random.Range(-inaccuracy * inaccuracyLerp, inaccuracy * inaccuracyLerp));

                    RaycastHit[] hits;

                    hits = Physics.RaycastAll(m_camera.transform.position, dir + inaccuracyVector, 1000.0f).OrderBy(h=>h.distance).ToArray();

                    //If something it hit then tell the bullet trail to move towards the end point of the hit else give it an angle offset to make it look accurate
                    if (hits.Length != 0)
                    {
                        offsetTrailAngle = Angles(hits[0].point);

                        int goThrough;

                        if (penetrationNo == 1)
                            goThrough = 1;
                        else if (hits.Length > penetrationNo)
                            goThrough = penetrationNo;
                        else
                            goThrough = hits.Length;

                        for (int j = 0; j < goThrough; j++)
                        {
                            //Do stuff depending on what it hit
                            if (hits[j].collider.gameObject.tag == "Damageable")
                            {
                                hits[j].transform.gameObject.GetComponent<Damageable>().TakeDamage(damage);
                                scriptManager.GetComponent<HitMarker>().HitDamageable();
                                //Deals with status effect procs on enemies
                                foreach (string s in ProcStatus())
                                {
                                    if (s == "Bleed")
                                    {
                                        hits[j].transform.gameObject.GetComponent<Damageable>().AddBleedStack(1 + skillTree[7].GetB());
                                    }

                                    if (s == "Cripple")
                                    {
                                        hits[j].transform.gameObject.GetComponent<Damageable>().Cripple();
                                    }

                                    if (s == "InstaKill")
                                    {
                                        hits[j].transform.gameObject.GetComponent<Damageable>().Kill();
                                    }
                                    if (s == "Fire")
                                    {
                                        hits[j].transform.gameObject.GetComponent<Damageable>().AddFireStack(1);
                                    }
                                }
                            }
                            else if (hits[j].collider.gameObject.tag == "Wall" || hits[j].collider.gameObject.tag == "Floor")
                            {
                                Instantiate(bulletImpact, hits[j].point + (hits[j].normal * 0.001f), Quaternion.FromToRotation(Vector3.up, hits[j].normal));
                                Instantiate(smoke, hits[j].point + (hits[j].normal * 0.001f), Quaternion.Euler(0.0f, 0.0f, 0.0f));
                            }
                        }

                        GameObject trail = (GameObject)Instantiate(m_trail, bulletLeave.transform.position, Quaternion.LookRotation(dir + inaccuracyVector));
                        trail.GetComponent<TrailForce>().SetEndPoint(hits[0].point);

                        //Create small impact explosion
                        Collider[] objsInRange = Physics.OverlapSphere(hits[0].point, 2.0f);

                        foreach (Collider col in objsInRange)
                        {
                            Rigidbody rb = col.GetComponent<Rigidbody>();

                            if (rb != null)
                            {
                                rb.AddExplosionForce(100.0f, hits[0].point, 5.0f);
                            }
                        }
                    }
                    else
                    {
                        offsetTrailAngle = 1.0f;
                        Instantiate(m_trail, bulletLeave.transform.position, Quaternion.LookRotation(dir + inaccuracyVector) * Quaternion.Euler(0, -offsetTrailAngle, 0));
                    }
                }

                //Muzzle flash
                GameObject mFlash = (GameObject)Instantiate(muzzleFlash, bulletLeave.transform.position, bulletLeave.transform.rotation * Quaternion.Euler(0, 0, Random.Range(0, 360)));
                mFlash.GetComponent<FollowBulletLeave>().SetBulletLeave(bulletLeave);

                //Smoke
                Instantiate(smoke, bulletLeave.transform.position, Quaternion.Euler(0.0f, 0.0f, 0.0f));

                canShoot = false;

                //Stuff for other scripts to make it more "gun-like"
                //parentGun.GetComponent<WeaponMovement>().ShotFalse();
                parentGun.GetComponent<WeaponMovement>().ShotTrue();

                if (parentGunRot != null)
                {
                    parentGunRot.GetComponent<WeaponRotate>().ShotTrue();
                }

                foreach (GameObject go in movingParts)
                {
                    //go.GetComponent<MovingParts>().ShotFalse();
                    go.GetComponent<MovingParts>().ShotTrue();
                }

                theCamera.GetComponent<CameraRecoil>().ShotTrue();

                //Setup inaccuracy for spraying
                if (scriptManager.GetComponent<GunManager>().GetActiveWeapon() != 2)
                {
                    inaccuracyLerp += timeGap * 10 * inaccuracyMultiplier;

                    if (inaccuracyLerp > 1.0f)
                    {
                        inaccuracyLerp = 1.0f;
                    }
                }
            }
        }
    }

    public void UpdateSkillTree(string name)
    {
        for (int i = 0; i < skillTree.Length; i++)
        {
            if (skillTree[i].GetA() == name)
            {
                skillTree[i].SetB(skillTree[i].GetB() + 1);
            }
        }
    }

    protected void PopulateExpBracket()
    {
        int baseExpReq = 200;

        for (int i = 0; i < 31; i++)
        {
            if (i == 31)
            {
                expBracket[i] = 1;
                break;
            }

            expBracket[i] = baseExpReq + (baseExpReq / 2) * i;
        }
    }

    public void GainExp(int no)
    {
        exp += no;

        CheckLevelUp();
    }

    public void CheckLevelUp()
    {
        if (level < 30)
        {
            while (exp >= expBracket[level])
            {
                int leftover;
                leftover = exp - expBracket[level];
                exp = leftover;

                level++;
                skillPoints++;
            }
        }
    }

    //Obselete but I can't bring myself to delete this
    public float Angles(Vector3 hitPoint)
    {
        float distOpposite;
        float distAdjacent;

        cameraRightAngle = m_camera.transform.position;
        cameraRightAngle.z = bulletLeave.transform.position.z;
        cameraRightAngle.x = bulletLeave.transform.position.x;

        distAdjacent = Vector3.Distance(cameraRightAngle, hitPoint);
        distOpposite = Vector3.Distance(cameraRightAngle, bulletLeave.transform.position);

        float result = Mathf.Rad2Deg * Mathf.Atan2(distOpposite, distAdjacent);

        return result;
    }

    public void SetMaxEnergy(int energy)
    {
        maxEnergy = energy;
    }

    public void SetCurrEnergy(int energy)
    {
        currEnergy = energy;
    }

    public int GetMaxEnergy()
    {
        return maxEnergy;
    }

    public int GetCurrEnergy()
    {
        return currEnergy;
    }

    public Pair<string, int>[] GetSkillTree()
    {
        return skillTree;
    }

    public int GetLevel()
    {
        return level;
    }

    public int GetCurrExp()
    {
        return exp;
    }

    public int GetCurrExpBracket()
    {
        return expBracket[level];
    }

    public int GetSkillPoints()
    {
        return skillPoints;
    }

    public void SetSkillPoints(int no)
    {
        skillPoints = no;
    }

    public void AddSkillPoints(int no)
    {
        skillPoints += no;
    }

    public abstract void AddEnergy(int e);

    protected abstract List<string> ProcStatus();

    protected abstract List<string> ProcBuff();
}