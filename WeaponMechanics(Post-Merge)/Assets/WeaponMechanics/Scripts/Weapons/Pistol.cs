﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pistol : Gun
{
    new void Start()
    {
        base.Start();

        maxEnergy = 999;
        currEnergy = 999;
        energyUse = 0;
        fireRate = 0.4f;
        damage = 5;
        inaccuracy = 0.05f;
        inaccuracyLerp = 0.0f;
        inaccuracyMultiplier = 25.0f * inaccuracy;
        noOfBulletsPerShot = 1;

        canShoot = true;

        skillTree = new Pair<string, int>[6];

        PopulateSkillTree();
    }

    new void Update()
    {
        base.Update();

        damage = 5 + (skillTree[1].GetB());

        fireRate = 0.40f - (skillTree[0].GetB() / 50.00f);
    }

    public override void AddEnergy(int e)
    {
        
    }

    protected override List<string> ProcStatus()
    {
        List<string> temp = new List<string>();

        if (skillTree[4].GetB() > 0)
        {
            if ((skillTree[4].GetB() * 2) > Random.Range(0, 100))
            {
                temp.Add("InstaKill");
            }
        }

        return temp;
    }

    protected override List<string> ProcBuff()
    {
        List<string> temp = new List<string>();

        if (skillTree[2].GetB() > 0)
        {
            if ((skillTree[2].GetB() * 5) > Random.Range(0, 100))
            {
                temp.Add("Lifesteal");
            }
        }

        if (skillTree[3].GetB() > 0)
        {
            if ((skillTree[3].GetB() * 5) > Random.Range(0, 100))
            {
                temp.Add("MoveSpeed");
            }
        }

        if (skillTree[5].GetB() > 0)
        {
            for (int i = 0; i < skillTree[5].GetB(); i++)
            {
                if ((skillTree[5].GetB() * 5) > Random.Range(0, 100))
                {
                    temp.Add("MultiShot");
                }
            }
        }

        return temp;
    }

    public void PopulateSkillTree()
    {
        skillTree[0] = new Pair<string, int>("Firerate", 0);    //DONE
        skillTree[1] = new Pair<string, int>("Damage", 0);      //DONE
        skillTree[2] = new Pair<string, int>("ChanceProcLifesteal", 0); //DONE
        skillTree[3] = new Pair<string, int>("ChanceProcMoveSpeedBuff", 0); //DONE
        skillTree[4] = new Pair<string, int>("ChanceProcInstaKill", 0); //DONE
        skillTree[5] = new Pair<string, int>("ChanceProcMultiShot", 0); //DONE

        //Augment: Double skill points for one skill (skill cap for that skill is also doubled)
    }
}