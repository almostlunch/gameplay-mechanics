﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Rifle : Gun
{
    new void Start()
    {
        base.Start();

        //maxEnergy = 100;
        //currEnergy = 100;
        energyUse = 1;
        fireRate = 0.08f;
        damage = 5;
        inaccuracy = 0.05f;
        baseFireRate = 0.15f;
        baseDamage = 5;
        baseInaccuracy = 0.05f;
        inaccuracyLerp = 0.0f;
        inaccuracyMultiplier = 25.0f * inaccuracy;
        noOfBulletsPerShot = 1;

        canShoot = true;

        skillTree = new Pair<string, int>[9];

        PopulateSkillTree();
    }

    new void Update()
    {
        base.Update();

        

        maxEnergy = WeaponEnergy.GetMaxEnergyRifle();
        currEnergy = WeaponEnergy.GetCurrEnergyRifle();

        damage = baseDamage + (skillTree[2].GetB()) + (skillTree[8].GetB() * 5);

        inaccuracy = baseInaccuracy - (skillTree[3].GetB() / 700.00f);
        //inaccuracyMultiplier = 15.0f * inaccuracy;

        fireRate = baseFireRate - (skillTree[4].GetB() / 250.00f);

        penetrationNo = 1 + skillTree[6].GetB();

        scriptManager.GetComponent<WeaponEnergy>().SetRifleRegen(skillTree[5].GetB());
    }

    public override void AddEnergy(int e)
    {
        WeaponEnergy.AddEnergyRifle(e);
    }

    protected override List<string> ProcStatus()
    {
        List<string> temp = new List<string>();
        
        if (skillTree[0].GetB() > 0)
        {
            if ((skillTree[0].GetB() * 5) > Random.Range(0, 100))
            {
                temp.Add("Bleed");
            }
        }

        return temp;
    }

    protected override List<string> ProcBuff()
    {
        List<string> temp = new List<string>();

        if (skillTree[1].GetB() > 0)
        {
            if ((skillTree[1].GetB() * 5) > Random.Range(0, 100))
            {
                temp.Add("NotUseENergy");
            }
        }

        return temp;
    }

    public void PopulateSkillTree()
    {
        skillTree[0] = new Pair<string, int>("ProcBleed", 0);   //DONE
        skillTree[1] = new Pair<string, int>("ChanceToNotUseEnergy", 0);    //DONE
        skillTree[2] = new Pair<string, int>("Damage", 0);  //DONE

        //LMG
        skillTree[3] = new Pair<string, int>("Accuracy", 0);    //DONE
        skillTree[4] = new Pair<string, int>("Firerate", 0);    //DONE
        skillTree[5] = new Pair<string, int>("EnergyRegen", 0); //DONE

        //DMR
        skillTree[6] = new Pair<string, int>("BulletPenetration", 0);   //DONE
        skillTree[7] = new Pair<string, int>("MoreProcBleed", 0);   //DONE
        skillTree[8] = new Pair<string, int>("MoreDamage", 0);  //DONE
    }

    public void Augment(string s)
    {
        if (s == "LMG")
        {
            baseFireRate = 0.05f;
            baseDamage = 5;
            baseInaccuracy = 0.08f;
        }
        else if (s == "DMR")
        {
            baseFireRate = 0.5f;
            baseDamage = 20;
            baseInaccuracy = 0.01f;
        }
    }

    public void SetIsRifleAug(string s)
    {
        if (s == "LMG")
            isRifleLMG = true;
        else if (s == "DMR")
            isRifleDMR = true;
    }

    public bool GetIsRifleAug(string s)
    {
        if (s == "LMG")
            return isRifleLMG;
        else if (s == "DMR")
            return isRifleDMR;
        else
            return false;
    }
}