﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Shotgun : Gun
{
    new void Start()
    {
        base.Start();

        //maxEnergy = 100;
        //currEnergy = 100;
        energyUse = 5;
        fireRate = 1.0f;
        damage = 5;
        inaccuracy = 0.07f;
        baseFireRate = 1.0f;
        baseDamage = 5;
        baseInaccuracy = 0.07f;
        inaccuracyLerp = 1.0f;
        inaccuracyMultiplier = 1.0f;
        noOfBulletsPerShot = 10;
        baseNoOfBulletsPerShot = 10;

        canShoot = true;

        skillTree = new Pair<string, int>[9];

        PopulateSkillTree();
    }

    new void Update()
    {
        base.Update();

        maxEnergy = WeaponEnergy.GetMaxEnergyShotgun();
        currEnergy = WeaponEnergy.GetCurrEnergyShotgun();

        damage = baseDamage + (skillTree[0].GetB());

        fireRate = baseFireRate - (skillTree[2].GetB() / 20.00f);

        inaccuracy = baseInaccuracy + (skillTree[5].GetB() / 100.00f) ;

        scriptManager.GetComponent<WeaponEnergy>().SetShotgunRegen(skillTree[8].GetB());
    }

    public override void AddEnergy(int e)
    {
        WeaponEnergy.AddEnergyShotgun(e);
    }

    protected override List<string> ProcStatus()
    {
        List<string> temp = new List<string>();

        if (skillTree[1].GetB() > 0)
        {
            if ((skillTree[1].GetB() * 5) > Random.Range(0, 100))
            {
                temp.Add("Cripple");
            }
        }

        return temp;
    }

    protected override List<string> ProcBuff()
    {
        List<string> temp = new List<string>();

        if (skillTree[5].GetB() > 0)
        {
            for (int i = 0; i < skillTree[5].GetB(); i++)
            {
                if ((skillTree[5].GetB() * 5) > Random.Range(0, 100))
                {
                    temp.Add("MultiShot");
                }
            }
        }

        return temp;
    }

    public void PopulateSkillTree()
    {
        skillTree[0] = new Pair<string, int>("Damage", 0);      //DONE
        skillTree[1] = new Pair<string, int>("ChanceToCrippleOnImpact", 0);     //DONE
        skillTree[2] = new Pair<string, int>("Firerate", 0);    //DONE

        //Grenade Launcher
        skillTree[3] = new Pair<string, int>("BiggerExplosion", 0);
        skillTree[4] = new Pair<string, int>("IgniteGroundOnExplosion", 0);
        skillTree[5] = new Pair<string, int>("ClusterBomb", 0);

        //Flame
        skillTree[6] = new Pair<string, int>("WiderSpread", 0);
        skillTree[7] = new Pair<string, int>("FireProcChance", 0);      //DONE
        skillTree[8] = new Pair<string, int>("EnergyRegen", 0);     //DONE
    }

    public void ShootFlame()
    {
        
    }

    public void ShootGrenade()
    {
        if (canShoot)
        {
            if (currEnergy > 0)
            {
                //currEnergy -= energyUse;
                AddEnergy(-energyUse);

                multiShot = 0;

                //Deals with procs for buffs
                foreach (string s in ProcBuff())
                {
                    if (s == "NotUseENergy")
                    {
                        AddEnergy(energyUse);
                    }
                    else if (s == "Lifesteal")
                    {
                        m_player.GetComponent<PlayerHealth>().Heal(5);
                    }
                    else if (s == "MoveSpeed")
                    {
                        m_player.GetComponent<PlayerController>().SpeedBuff();
                    }
                    else if (s == "MultiShot")
                    {
                        multiShot++;
                    }
                }
                


                for (int i = 0; i < noOfBulletsPerShot + multiShot; i++)
                {
                    Vector3 dir = m_camera.transform.rotation * Vector3.forward;
                    Vector3 inaccuracyVector = new Vector3(
                        Random.Range(-inaccuracy, inaccuracy),
                        Random.Range(-inaccuracy, inaccuracy),
                        Random.Range(-inaccuracy, inaccuracy));

                    Instantiate(grenade, bulletLeave.transform.position, Quaternion.LookRotation(dir + inaccuracyVector) * Quaternion.Euler(0, -offsetTrailAngle, 0));
                }    

                //Muzzle flash
                GameObject mFlash = (GameObject)Instantiate(muzzleFlash, bulletLeave.transform.position, bulletLeave.transform.rotation * Quaternion.Euler(0, 0, Random.Range(0, 360)));
                mFlash.GetComponent<FollowBulletLeave>().SetBulletLeave(bulletLeave);

                //Smoke
                Instantiate(smoke, bulletLeave.transform.position, Quaternion.Euler(0.0f, 0.0f, 0.0f));

                canShoot = false;

                //Stuff for other scripts to make it more "gun-like"
                //parentGun.GetComponent<WeaponMovement>().ShotFalse();
                parentGun.GetComponent<WeaponMovement>().ShotTrue();

                if (parentGunRot != null)
                {
                    parentGunRot.GetComponent<WeaponRotate>().ShotTrue();
                }

                foreach (GameObject go in movingParts)
                {
                    //go.GetComponent<MovingParts>().ShotFalse();
                    go.GetComponent<MovingParts>().ShotTrue();
                }

                theCamera.GetComponent<CameraRecoil>().ShotTrue();
            }
        }
    }

    public void Augment(string s)
    {
        if (s == "Flame")
        {
            energyUse = 1;
            baseFireRate = 0.2f;
            baseDamage = 5;
            baseInaccuracy = 0.07f;
        }
        else if (s == "Grenade")
        {
            baseFireRate = 1.0f;
            baseDamage = 5;
            baseInaccuracy = 0.02f;
            noOfBulletsPerShot = 1;
        }
    }

    public void SetIsShotgunAug(string s)
    {
        if (s == "Flame")
            isShotgunFlame = true;
        else if (s == "Grenade")
            isShotgunGrenade = true;
    }

    public bool GetIsShotgunAug(string s)
    {
        if (s == "Flame")
            return isShotgunFlame;
        else if (s == "Grenade")
            return isShotgunGrenade;
        else
            return false;
    }
}